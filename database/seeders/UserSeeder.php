<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'name' => 'Admin Greenk',
            'username' => 'admingreenk',
            'email' => 'admin.greenk@mail.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
            'remember_token' => Str::random(10),
        ]);
        $admin->roles()->sync([1,2]);

        $admin_two = User::create([
            'name' => 'Admin',
            'username' => 'admin',
            'email' => 'admin.article@mail.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
            'remember_token' => Str::random(10),
        ]);
        $admin_two->roles()->sync([1,2]);

        $writer = User::create([
            'name' => 'Tere Liye',
            'username' => 'tere_liye',
            'email' => 'tereliye@mail.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
            'remember_token' => Str::random(10),
        ]);
        $writer->roles()->sync([2]);

        $writer_two = User::create([
            'name' => 'Andrea Hirata',
            'username' => 'andreahirata',
            'email' => 'andreahirata@mail.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
            'remember_token' => Str::random(10),
        ]);
        $writer_two->roles()->sync([2]);

    }
}
