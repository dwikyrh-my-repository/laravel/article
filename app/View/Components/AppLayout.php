<?php

namespace App\View\Components;

use App\Enums\ArticleStatus;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\View\Component;

class AppLayout extends Component
{
    public $title;
    /**
     * Create a new component instance.
     *
     * @return void
     */

    public function __construct($title = 'Default Title')
    {
        $this->title = $title;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $categories = \App\Models\Category::query()
        ->select('name', 'slug')
        ->whereHas('articles', function(Builder $query) {
            $query->where('status', ArticleStatus::PUBLISHED);
        })
        ->get();
        return view('layouts.app', compact('categories'));
    }
}
