<?php

namespace App\Providers;

// use Illuminate\Support\Facades\Gate;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        'App\Models\Article' => 'App\Policies\ArticlePolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::before(fn ($user, $ability) => $user->isAdmin() ? true : null);

        Blade::directive('hasAnyRoles', fn ($roles) => "<?php if (auth()->user()->hasAnyRoles({$roles})): ?>");
Blade::directive('endHasAnyRoles', fn () => '<?php endif ?>');

Blade::directive('hasRole', fn ($role) => "<?php if (auth()->user()->hasRole($role)): ?>");
Blade::directive('endHasRole', fn () => '<?php endif ?>');
}
}