<?php

namespace App\Models;

use App\Traits\HasRole;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable, HasRole;

    protected $fillable = [
        'name',
        'username',
        'email',
        'password',
    ];


    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    # get route key name
    public function getRouteKeyName()
    {
        return 'username';
    }

    # relation table
    public function articles()
    {
        return $this->hasMany(Article::class, 'user_id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function likes()
    {
        return $this->hasMany(Like::class);
    }

    # toggle like
    public function toggleLike($model)
    {
        $exists = $this->likes()->whereMorphedTo('likeable', $model)->exists();
        if (!$exists) {
            $this->likes()->save($model->likes()->make());
        } else {
            $this->likes()->whereMorphedTo('likeable', $model)->delete();
        }
    }

}
