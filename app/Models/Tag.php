<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'slug'];

    # get route key name
    public function getRouteKeyName()
    {
        return 'slug';
    }

    # published article
    public function scopePublished($query)
    {
        return $query->where('status', \App\Enums\ArticleStatus::PUBLISHED);
    }

    # relation table
    public function articles()
    {
        return $this->belongsToMany(Article::class);
    }
}
