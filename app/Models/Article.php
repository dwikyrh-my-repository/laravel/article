<?php

namespace App\Models;

use App\Traits\HasLike;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Laravel\Scout\Searchable;

class Article extends Model
{
    use HasFactory, HasLike, Searchable;
    protected $table = 'articles';

    # fillable
    protected $fillable = ['user_id', 'category_id', 'title', 'slug', 'body', 'picture', 'published_at'];

    protected $casts = [
        'status' => \App\Enums\ArticleStatus::class,
        'published_at' => 'datetime',
    ];

    # with
    protected $with = ['user', 'category', 'tags'];

    # get route key name
    public function getRouteKeyName()
    {
        return 'slug';
    }

    # published article
    public function scopePublished($query)
    {
        return $query->where('status', \App\Enums\ArticleStatus::PUBLISHED);
    }

    # relation table
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    # laravel scout
    public function toSearchableArray()
    {
        $array = $this->toArray();

        return $array;
    }

}