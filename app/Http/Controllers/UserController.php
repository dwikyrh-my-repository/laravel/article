<?php

namespace App\Http\Controllers;

use App\Enums\ArticleStatus;
use App\Models\Article;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    # method __construct for middleware
    public function __construct()
    {
        $this->middleware(['auth', 'verified'])->except('show');
    }

    # get all data users
    public function index(Request $request)
    {
        $users = User::query()
            ->whereNotNull('email_verified_at')
            ->whereNot('id', $request->user()->id)
            ->with(['roles'])
            ->latest()
            ->paginate(10);

        # return view
        $roles = Role::select('id', 'name')->get();
        # return view
        return view('users.index', compact('users', 'roles'));
    }

    # show articles by user
    public function show(User $user)
    {
        // $articles = Article::query()->where('user_id', $user->id)->latest()->paginate(3);
        $articles = Article::query()->whereBelongsTo($user)->where('status', ArticleStatus::PUBLISHED)->latest()->paginate(9);
        return view('users.show', compact('user', 'articles'));
    }

    # view edit profile user
    public function edit(Request $request)
    {
        $users = $request->user();

        # return view
        return view('users.edit', compact('users'));
    }

    # process update profile user
    public function update(Request $request)
    {
        # validation
        $request->validate([
            'name' => ['required', 'string', 'min:3', 'max:100'],
            'email' => ['required', 'email', Rule::unique('users', 'email')->ignore($request->user())],
            'username' => ['required', 'string', 'min:3', 'max:25', 'alpha_num', Rule::unique('users', 'username')->ignore($request->user())],
        ]);
        # process update profile user
        $request->user()->update([
            'name' => $request->name,
            'email' => $request->email,
            'username' => $request->username,
        ]);

        # redirect
        return back()->with('success', 'User profile has been updated ');
    }
}