<?php

namespace App\Http\Controllers;

use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;

class EmailVerificationNotificationController extends Controller
{
    # view verified email
    public function notice(Request $request)
    {
        # condition if user has been verified and not yet verified email
        return $request->user()->hasVerifiedEmail()
        ? redirect()->intended(RouteServiceProvider::HOME) : view('auth.verify-email');
    }

    # process send verification email
    public function sendVerification(Request $request)
    {
        # if user has been verified email
        if ($request->user()->hasVerifiedEmail()) {
            return redirect()->intended(RouteServiceProvider::HOME);
        }

        # if user not yet verified email
        $request->user()->sendEmailVerificationNotification();

        # send alert to user
        session()->flash('status', 'Link verification email has been sent into your email address.');

        # redirect
        return back();
    }
}