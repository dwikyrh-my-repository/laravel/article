<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangePasswordRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class ChangePasswordController extends Controller
{
    # method __construct for middleware
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    # view edit change password user
    public function edit()
    {
        # return view
        return view('password.edit');
    }

    # process update change password user
    public function update(ChangePasswordRequest $request)
    {
        # check password when current password same in database
        if (!Hash::check($request->current_password, $request->user()->password))
        {
            throw ValidationException::withMessages([
                'current_password' => 'The provided password does not match your current password.',
            ]);
        }

        # check password when new password same in database
        if (Hash::check($request->password, $request->user()->password)) {
            throw ValidationException::withMessages([
                'password' => 'The new password cannot be the same as the old password',
            ]);
        }

        # action change password user
        Auth::user()->update(['password' => bcrypt($request->password)]);

        # redirect
        return back()->with('success', 'Password has been changed ');
    }
}
