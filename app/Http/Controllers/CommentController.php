<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    # process create comments user
    public function store(Request $request, Article $article)
    {
        # validation
        $attributes = $request->validate([
            'body' => ['required'],
        ]);

        # action create comment user
        $request->user()->comments()->save(
            $article->comments()->make($attributes)
        );

        # redirect
        return back()->with('success', 'Your comment has been created ');;
    }

    # process delete comment
    public function destroy(Request $request, Comment $comment)
    {
        # check if comment->user_id not $request->user()->id
        abort_if(
            $comment->user_id != $request->user()?->id,
            403,
            'You are not authorized.'
        );
        # action delete comment
        $comment->delete();

        # redirect
        return back()->with('success', 'Your comment has been deleted ');
    }
}