<?php

namespace App\Http\Controllers;

use App\Http\Requests\TagRequest;
use App\Models\Tag;

class TagController extends Controller
{
    # method __construct for middleware
    public function __construct()
    {
        return $this->middleware(['only.admin', 'auth', 'verified', 'preventBackHistory'])->except('show');
    }

    # show all data tags
    public function index()
    {
        # action show all data tags
        $tags = Tag::select('name', 'slug')->latest()->paginate(10);

        # return view
        return view('tags.index', compact('tags'));
    }

    # view create tag
    public function create()
    {
        $tag = new Tag();
        # return view
        return view('tags.create', compact('tag'));
    }

    # process create tag
    public function store(TagRequest $request)
    {
        # action create tag
        Tag::create([
            'name' => $name = $request->name,
            'slug' => str($name . ' ' . str()->random(3))->slug(),
        ]);
        return to_route('tags.index')->with('success', 'Data tag has been created ');
    }

    # show articles by tag
    public function show(Tag $tag)
    {
        # action get articles by tag
        $articles = $tag->articles()->published()->paginate(15);

        # return view
        return view('tags.show', compact('tag', 'articles'));
    }

    # view edit tag
    public function edit(Tag $tag)
    {
        return view('tags.edit', compact('tag'));
    }

    # process update tag
    public function update(TagRequest $request, Tag $tag)
    {
        # action update tag
        $tag->update([
            'name' => $name = $request->name,
            'slug' => str($name . ' ' . str()->random(3))->slug(),
        ]);

        # redirect
        return to_route('tags.index')->with('success', 'Data tag has been updated ');
    }

    # process delete tag
    public function destroy(Tag $tag)
    {
        # action delete tag
        $tag->delete();

        # redirect
        return to_route('tags.index')->with('success', 'Data tag has been deleted ');
    }
}