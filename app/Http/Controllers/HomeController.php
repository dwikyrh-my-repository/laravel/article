<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    # view home articles
    public function __invoke()
    {
        # get all data articles
        $articles = Article::query()->latest()->where('status', \App\Enums\ArticleStatus::PUBLISHED)->limit(6)->get();

        # get name user
        $user = auth()->user()?->name;

        # return view
        return view('home', compact('articles', 'user'));
    }
}
