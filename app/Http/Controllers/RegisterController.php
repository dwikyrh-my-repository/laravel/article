<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class RegisterController extends Controller
{
    # view register form user
    public function showRegistrationForm()
    {
        # return view
        return view('auth.register');
    }

    # process register user
    public function registerUser(Request $request)
    {
        # validation
        $request->validate([
            'name' => ['required'],
            'email' => ['required', Rule::unique('users', 'email')],
            'password' => ['required', 'min:8', 'max:128', 'confirmed'],
        ]);

        # create user and save to db
        tap(User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]), function($user) {
            if ($user instanceof MustVerifyEmail && !$user->hasVerifiedEmail()) {
                $user->sendEmailVerificationNotification();
            }
            # Update username
            $user->update(['username' => $user->id . now()->timestamp]);
            Auth::login($user);
        });

        # redirect to home
        return redirect(RouteServiceProvider::HOME);
    }
}
