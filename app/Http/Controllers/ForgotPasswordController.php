<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Validation\ValidationException;

class ForgotPasswordController extends Controller
{
    # view forgot password
    public function create()
    {
        # return view forgot password
        return view('auth.forgot-password');
    }

    # process send email for forgot password
    public function store(Request $request)
    {
        # validation
        $request->validate([
            'email' => ['required', 'email'],
        ]);

        # action forgot password
        $status = Password::sendResetLink(
            $request->only('email')
        );

        if ($status == Password::RESET_LINK_SENT) {
            return redirect()->back()->with('status', __($status));
        }

        throw ValidationException::withMessages([
            'email' => [trans($status)],
        ]);
    }
}