<?php

namespace App\Http\Controllers;

use App\Enums\ArticleStatus;
use App\Http\Requests\ArticleRequest;
use App\Models\Article;
use App\Models\Category;
use App\Models\Comment;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ArticleController extends Controller
{
    # method __construct for middleware, data categories, and data tags
    public function __construct()
    {
        $this->middleware(['auth', 'verified', 'can.write.article', 'preventBackHistory'])->except(['show', 'index', 'search']);
        $this->categories = Category::select('id', 'name')->get();
        $this->tags = Tag::select('id', 'name')->get();
    }

    # process search articles
    public function search(Request $request)
    {
        # validation
        $request->validate([
            'q' => ['required']
        ]);

        # action get articles buy title, user, category, and tags
        $articles = Article::query()->latest()->when(request()->q, function ($articles) {
            $articles->where('title', 'like', '%' . request()->q . '%')->where('status', ArticleStatus::PUBLISHED)
                ->orWhereHas('user', function (Builder $query) {
                    $query->where('name', 'like', '%' . request()->q . '%')->where('status', ArticleStatus::PUBLISHED);
                })
                ->orWhereHas('category', function (Builder $query) {
                    $query->where('name', 'like', '%' . request()->q . '%')->where('status', ArticleStatus::PUBLISHED);
                })
                ->orWhereHas('tags', function (Builder $query) {
                    $query->where('name', 'like', '%' . request()->q . '%')->where('status', ArticleStatus::PUBLISHED);
                });
        })->get();

        # return view
        return view('articles.search', compact('articles'));
    }

    # table of articles
    public function table(Request $request)
    {
        # table of articles
        $articles = Article::without(['category', 'tags'])
            ->when(!$request->user()->isAdmin(), fn ($query) => $query->whereBelongsTo($request->user()))
            ->latest()
            ->paginate(10);

        # return view
        return view('articles.table', compact('articles'));
    }

    # update status article (pending, rejected, published)
    public function updateStatus(Request $request, Article $article)
    {
        # action change status article
        $article->forceFill([
            'status' => $request->status,
            'published_at' => now(),
        ])->save();

        # redirect
        return back();
    }

    # show all articles
    public function index()
    {
        # action get all data articles
        $articles = Article::query()->where('status', ArticleStatus::PUBLISHED)->latest()->paginate(15);

        # return view
        return view('articles.index', compact('articles'));
    }

    # view edit article
    public function create()
    {
        # get data categories, tags, and article
        $categories = $this->categories;
        $tags = $this->tags;
        $article = new Article();

        # return view
        return view('articles.create', compact('categories', 'tags', 'article'));
    }

    # process create article
    public function store(ArticleRequest $request)
    {
        # action create article
        $fileRequest = $request->file('picture');
        $article = Auth::user()->articles()->create([
            'title' => $title = $request->title,
            'slug' => $slug = str($title . ' ' . str()->random(3))->slug(),
            'body' => $request->body,
            'category_id' => $request->category,
            'picture' => $request->hasFile('picture')
                ? $fileRequest->storeAs('articles/images', $slug . '.' . $fileRequest->extension()) : null,
        ]);
        # create tags handle in article
        $article->tags()->attach($request->tags);

        # redirect
        return to_route('articles.show', $article)->with('success', 'Data article has been created ');
    }

    # show article spesific
    public function show(Article $article)
    {
        // $relatedArticles = Article::query()->where('category_id', $article->category_id)->where('id', '!=', $article->id)
        // ->latest()->limit(9)->get();

        # related articles
        $relatedArticles = Article::query()
            ->whereBelongsTo($article->category)
            ->published()
            ->whereNot('id', $article->id)
            ->latest()
            ->limit(9)
            ->get();

        # comments user in spesific article
        $comments = Comment::query()->select('user_id', 'body', 'created_at', 'id')->with('user')
            ->withCount('likes')->whereMorphedTo('commentable', $article)->get();

        # policy
        $this->authorize('viewIfOnlyAuthorOrAdmin', $article);

        # get count likes article spesific
        $article = $article->loadCount('likes');

        # return view
        return view('articles.show', compact('relatedArticles', 'comments', 'article'));
    }

    # view edit article
    public function edit(Article $article)
    {
        # policy
        $this->authorize('view', $article);

        # get data categories and tags
        $categories = $this->categories;
        $tags = $this->tags;

        # return view
        return view('articles.edit', compact('article', 'categories', 'tags'));
    }

    # process update article
    public function update(ArticleRequest $request, Article $article)
    {
        # policy
        $this->authorize('update', $article);

        # picture article handle
        $title = $request->title;
        $slug = str($title . ' ' . str()->random(5))->slug();
        $fileRequest = $request->file('picture');

        if ($request->hasFile('picture')) {
            if ($article->picture) {
                Storage::delete($article->picture);
            }
            $picture = $fileRequest->storeAs('articles/images', $slug . '.' . $fileRequest->extension());
        } else {
            $picture = $article->picture;
        }

        # action edit article
        $article->update([
            'title' => $title,
            'slug' => $slug,
            'body' => $request->body,
            'category_id' => $request->category,
            'picture' => $picture,
        ]);

        # update tag handle
        $article->tags()->sync($request->tags, true);

        # redirect
        return redirect()->route('articles.show', $article)->with('success', 'Data article has been updated ');
    }

    # process delete article
    public function destroy(Article $article)
    {
        # policy
        $this->authorize('delete', $article);

        # delete picture article in storage
        if ($article->picture) {
            Storage::delete($article->picture);
        }

        # detach tags (delete tags in pivot table)
        $article->tags()->detach();

        # action delete article
        $article->delete();

        # redirect
        return redirect()->route('articles.index')->with('success', 'Data article has been deleted ');
    }
}