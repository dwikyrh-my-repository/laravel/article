<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    # view login form
    public function showLoginForm()
    {
        return view('auth.login');
    }

    # process login user
    public function loginUser(Request $request)
    {
        # validate
        $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);


        # process login user
        if(!Auth::attempt($request->only('email', 'password'), $request->boolean('remember'))) {
            throw ValidationException::withMessages([
                'password' => 'These credentials do not match our records.'
            ]);
        }
        $request->session()->regenerate();

        # redirect
        return to_route('home');
    }
}
