<?php

namespace App\Http\Controllers;

use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Verified;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\Request;

class VerifyEmailController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    # change status to verified email
    public function __invoke(EmailVerificationRequest $request)
    {
        # when user has been verified email
        if ($request->user()->hasVerifiedEmail()) {
            return redirect()->intended(RouteServiceProvider::HOME.'?verified=1');
        }

        # make email verified to user
        if ($request->user()->markEmailAsVerified()) {
            event(new Verified($request->user()));
        }

        # redirect
        return redirect()->intended(RouteServiceProvider::HOME.'?verified=1');
    }
}
