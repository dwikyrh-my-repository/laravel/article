<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CategoryController extends Controller
{
    # method __construct for middleware
    public function __construct()
    {
        return $this->middleware(['only.admin', 'auth', 'verified', 'preventBackHistory'])->except('show');
    }

    # show all data categories
    public function index()
    {
        # action show all data categories
        $categories = Category::query()->latest()->paginate(10);

        # return view
        return view('categories.index', compact('categories'));
    }

    # view create category
    public function create()
    {
        $category = New Category();
        return view('categories.create', compact('category'));
    }

    # process create category
    public function store(CategoryRequest $request)
    {
        # action create category
        Category::create([
            'name' => $name = $request->name,
            'slug' => str($name . ' ' . str()->random(3))->slug(),
        ]);

        # redirect
        return to_route('categories.index')->with('success', 'Data category has been created ');
    }

    # show articles by category
    public function show(Category $category)
    {
        // $articles = $category->articles()->paginate(9);
        # get data articles by category when article has been published
        $articles = $category->articles()->published()->latest()->paginate(15);

        # return view
        return view('categories.show', compact('category', 'articles'));
    }

    # view edit category
    public function edit(Category $category)
    {
        # return view
        return view('categories.edit', compact('category'));
    }

    # process update category
    public function update(CategoryRequest $request, Category $category)
    {
        # action update category
        $category->update([
            'name' => $name = $request->name,
            'slug' => str($name . ' ' . str()->random(3))->slug(),
        ]);

        # redirect
        return to_route('categories.index')->with('success', 'Data category has been updated ');
    }

    # process delete category
    public function destroy(Category $category)
    {
        # action delete category
        $category->delete();

        # redirect
        return to_route('categories.index')->with('success', 'Data category has been deleted ');
    }
}