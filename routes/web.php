<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ChangePasswordController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LikeController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\TagController;
use App\Http\Controllers\UserController;
use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

# Home
Route::get('/', HomeController::class)->name('home');

// # Users
Route::get('/users', [UserController::class, 'index'])->name('users.index');
Route::get('/users/{user}', [UserController::class, 'show'])->name('users.show');

# Approvement
Route::controller(ArticleController::class)->middleware('can.write.article')->group(function() {
    # Table
    Route::get('articles/table', [ArticleController::class, 'table'])->name('articles.table');
    # Update Status
    Route::put('articles/{article}/update-status', [ArticleController::class, 'updateStatus'])->name('articles.update-status');
});


# Article Search
Route::get('articles/search', [ArticleController::class, 'search'])->name('articles.search');

# Articles
Route::resource('articles', ArticleController::class);

# Category
Route::resource('categories', CategoryController::class);

# Tags
Route::resource('tags', TagController::class);


Route::middleware('auth')->group(function() {
    # Comments
    Route::controller(CommentController::class)->group(function () {
        Route::post('comments/{article}', 'store')->name('comments.store');
        Route::delete('comments/{comment}', 'destroy')->name('comments.destroy');
    });
    # Likes
    Route::controller(LikeController::class)->group(function() {
        Route::post('like-comment/{comment}', 'likeComment')->name('comments.like');
        Route::post('like-article/{article}', 'likeArticle')->name('articles.like');
    });
});


# Auth
require __DIR__ . '/auth.php';

Route::controller(RoleController::class)->middleware('only.admin')->group(function () {
    Route::post('roles/assign/{user}', 'assign')->name('roles.assign');
});

# Account
Route::controller(UserController::class)->group(function() {
    Route::get('users', 'index')->name('users.index');
    Route::get('/account/edit', 'edit')->name('users.edit');
    Route::put('/account/edit', 'update')->name('users.update');
    Route::get('/{user}', [UserController::class, 'show'])->name('users.show');
});

# Change Password
Route::controller(ChangePasswordController::class)->middleware('auth')->group(function () {
    Route::get('account/password-edit', 'edit')->name('change-password.edit');
    Route::put('account/password-edit', 'update')->name('change-password.update');
});

// # Users
Route::get('/{user}', [UserController::class, 'show'])->name('user.show');