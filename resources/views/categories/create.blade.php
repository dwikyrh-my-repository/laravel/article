<x-app-layout title="Create New Category - Article">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <x-card title="New" subtitle="Create new category" class="shadow">
          {{-- form create category --}}
          <form action="{{ route('categories.store') }}" method="POST">
            @include('categories.form', ['submit' => 'Create'])
          </form>
        </x-card>
      </div>

      {{-- table of categories --}}
      <div class="col-md-4">
        <a class="btn btn-primary w-100" href="{{ route('categories.index') }}">
          Table of categories
        </a>
      </div>

    </div>
  </div>
</x-app-layout>
