<x-app-layout title="{{ $category->name }} Category - GreenK Article">
    <x-header :title="$category->name" subtitle="This page will show all articles by {{ $category->name }} category" />
    <div class="container">
        <x-articles :articles="$articles" />
    </div>
</x-app-layout>
