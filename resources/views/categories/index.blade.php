<x-app-layout title="Table of Categories - GreenK Article">
  @push('styles')
  @include('components.toast.alerts-css')
  @endpush
  <div class="container">
    <div class="row d-flex align-items-center justify-content-center">
      <div class="col-md-8">
        <div class="d-flex justify-content-end mb-2">
          <a href="{{ route('categories.create') }}" class="btn btn-primary">New</a>
        </div>
        <x-card title="Categories" subtitle="Table of categories" class="shadow">
          {{-- Table of Categories --}}
          <table class="table table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th>Category Name</th>
                <th>Slug</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @forelse($categories as $index => $category)
              <tr>
                <td>{{ $index + 1 }}</td>
                <td>{{ $category['name'] }}</td>
                <td>{{ $category['slug'] }}</td>
                <td>
                  <a href="{{ route('categories.edit', $category) }}" class="btn btn-warning btn-sm">
                    Edit
                  </a>
                </td>
              </tr>
              @empty
              <tr>
                <td colspan="4">Category does not exist</td>
              </tr>
              @endforelse
            </tbody>
          </table>
        </x-card>
        <div class="mt-2">{{ $categories->links() }}</div>
      </div>
    </div>
  </div>
  @push('scripts')
  @include('components.toast.alerts-js')
  @endpush
</x-app-layout>
