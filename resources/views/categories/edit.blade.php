<x-app-layout title="Edit Category: {{ $category->name }} - GreenK Article">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <x-card title="Edit" class="shadow">
          {{-- form edit article category --}}
          <form action="{{ route('categories.update', $category) }}" method='POST'>
            @method('PUT')
            @include('categories.form', ['submit' => 'Update'])
          </form>
        </x-card>
      </div>

      <div class="col-md-4">
        {{-- Table of Categories --}}
        <a class="btn btn-primary w-100" href="{{ route('categories.index') }}">
          Table of categories
        </a>

        {{-- Delete Button Modal --}}
        <button type="button" class="btn btn-danger w-100 mt-2" data-bs-toggle="modal" data-bs-target="#exampleModal">
          Delete
        </button>
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Category: {{ $category->name }}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                <p class="text-muted text-center">
                  Are you really sure you want to delete it?
                </p>
                <div class="d-flex align-items-center gap-2 justify-content-center">
                  <form action="{{ route('categories.destroy', $category) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger btn-md">Yes</button>
                    <button type="button" class="btn btn-secondary btn-md" data-bs-dismiss="modal" aria-label="Close">No</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</x-app-layout>
