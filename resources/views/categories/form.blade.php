@csrf
{{-- category name --}}
<div class="mb-4">
  <label for="name" class="form-label">Category Name</label>
  <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name', $category->name) }}">
  @error('name')
  <div class="invalid-feedback">{{ $message }}</div>
  @enderror
</div>

{{-- button submit --}}
<button type="submit" class="btn btn-primary btn-md">{{ $submit }}</button>
