{{-- footer --}}
<footer class="bg-white text-center text-white border-top mt-4">
  <div class="text-center py-4">
    <span class="text-dark"><strong>©2022 GreenK Article.</strong> All Rights Reserved.</span>
  </div>
</footer>
