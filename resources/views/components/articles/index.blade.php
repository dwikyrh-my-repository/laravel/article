{{-- Article Card --}}
@empty(!$articles)
<div class="row mb-2">
  @foreach ($articles as $article)
  <x-articles.single :article="$article" />
  @endforeach
</div>
@endempty

@if($articles->count() == null)
<div class="alert alert-info">No data articles.</div>
@endif

@if ($articles instanceof \Illuminate\Pagination\LengthAwarePaginator)
{{ $articles->links() }}
@endif
