@push('styles')
{{-- aos css --}}
<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
@endpush
<div class="col-12 col-sm-6 col-lg-4 mb-4">
  <div class="border shadow rounded-4 overflow-hidden bg-white" style="min-height: 475px" data-aos="fade-up">
    {{-- image --}}
    <div class="picture-article">
      <a href="{{ route('articles.show', $article) }}">
        <img src="{{ (substr($article->picture, 0, 5) == 'https') ? $article->picture : asset('storage/' . $article->picture)}}" alt="" class="w-100 bg-light" style="height: 250px;">
      </a>
    </div>
    <div class="p-4">

      <div class="d-flex flex-wrap justify-content-between">
        {{-- write / user --}}
        <small>
          <a href="{{ route('users.show', $article->user) }}" class="text-decoration-none text-muted float-end">
            <small> {{ $article->user->name }}</small>
          </a>
        </small>

        {{-- published / created_at --}}
        <smal>
          <small class="card-subtitle text-muted">{{ $article->created_at->format('d F, Y') }}</small>
        </smal>

      </div>
      {{-- article title --}}
      <a href="{{ route('articles.show', $article) }}" class="d-block fw-bolder text-dark text-decoration-none mt-3">
        {{ str($article->title)->limit(50) }}
      </a>

      <div class="d-flex flex-column align-items-start mt-2">
        {{-- category --}}
        <a href="{{ route('categories.show', $article->category) }}" class="text-decoration-none btn btn-primary btn-sm">
          {{ $article->category->name }}
        </a>
        {{-- Tags --}}
        <small class="mt-2 d-flex flex-column align-self-start">
          @empty(!$article->tags)
          <small class="d-block">
            @foreach($article->tags as $tag)
            <a href="{{ route('tags.show', $tag) }}" class="text-decoration-none px-1 text-dark">#{{ $tag->name }}</a>
            @endforeach
          </small>
          @endempty
        </small>
      </div>
    </div>
  </div>
</div>
@push('scripts')
{{-- AOS JS --}}
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script>
  AOS.init();

</script>
@endpush
