@if ($comments->count())
@foreach ($comments as $comment)
<div class="mb-4 border-top pt-4 mt-4">
    <h6>{!! str($comment->body)->markdown() !!}</h6>
    <small class="text-muted d-flex align-items-center gap-2">
        {{ $comment->user->name }} &middot; {{ $comment->created_at->diffForHumans() }}
        @auth
        {{-- Number of likes --}}
        <span>
            {{ $comment->likes_count }} {{ str('like')->plural($comment->likes_count) }}
        </span>
        {{-- Comment Like --}}
        &middot;
        <form action="{{ route('comments.like', $comment) }}" method="POST">
            @csrf
            <a href="{{ route('comments.like', $comment) }}" class="text-primary text-decoration-none"
                onclick="event.preventDefault();this.closest('form').submit();">
                {{ $comment->alreadyLiked() ? 'Unlike' : 'Like' }}
            </a>
        </form>
        {{-- Delete Comment --}}
        @if(Auth::user()?->id === $comment->user_id) &middot;
        <form action="{{ route('comments.destroy', $comment) }}" method="POST">
            @csrf
            @method('DELETE')
            <a href="{{ route('comments.destroy', $comment) }}" class="text-danger text-decoration-none"
                onclick="event.preventDefault();this.closest('form').submit()">
                Delete
            </a>
        </form>
        @endif
        @endauth
    </small>
</div>
@endforeach
@endif
<div class="mt-4 mb-3">
    <hr class="border-secondary">
</div>
