@auth
{{-- Form Comment --}}
<form action="{{ route('comments.store', $article) }}" method="POST" class="mb-4">
@csrf
{{-- Comment --}}
<textarea name="body" id="body" cols="30" rows="4" placeholder="Hi {{ auth()->user()?->username }}. What's on your mind" class="form-control"></textarea>
<div class="text-start mt-2">
    <button type="submit" class="btn btn-primary">Comment</button>
</div>
</form>
@else
<div class="mb-5">
    <a href="{{ route('login') }}" class="text-decoration-none">Login</a> to make a comment
</div>
@endauth
