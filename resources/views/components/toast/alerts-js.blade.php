<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/toastify-js"></script>
<script>
@if(session('success'))
Toastify({
  text: '{{ session('success') }}',
  duration: 4000,
  destination: "https://github.com/apvarun/toastify-js",
  newWindow: true,
  close: true,
  gravity: "top", // `top` or `bottom`
  position: "right", // `left`, `center` or `right`
  stopOnFocus: true, // Prevents dismissing of toast on hover
  style: {
    background: "linear-gradient(90deg, #1488cc 0%, #2b32b2 100%)",
  },
  onClick: function(){} // Callback after click
}).showToast();
@endif
</script>
