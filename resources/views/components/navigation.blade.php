<nav class="navbar navbar-expand-lg mb-4 shadow-sm">
  <div class="container">
    {{-- navbar brand --}}
    <a class="navbar-brand fw-bolder" href="{{ route('home') }}">GREENK ARTICLE</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      {{-- main navigation --}}
      <ul class="navbar-nav me-auto">
        {{-- home --}}
        <li class="nav-item">
          <a class="nav-link {{ (request()->is('/')) ? 'active' : '' }}" aria-current="page" href="{{ route('home') }}">Home</a>
        </li>
        {{-- all data articles --}}
        <li class="nav-item">
          <a class="nav-link {{ (request()->is('articles*')) ? 'active' : '' }}" href="{{ route('articles.index') }}">Articles</a>
        </li>
        {{-- categories --}}
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle {{ (request()->is('categories*')) ? 'active' : '' }}" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">Categories</a>
          <ul class="dropdown-menu">
            @foreach ($categories as $category)
            <li>
              <a class="dropdown-item" href="{{ route('categories.show', $category) }}">
                {{ $category->name }}
              </a>
            </li>
            @endforeach
          </ul>
        </li>
      </ul>
      {{-- dropdown menu --}}
      <ul class="navbar-nav me-md-4">
        @auth
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">
            {{-- user --}}
            {{ Auth::user()->name }}
          </a>
          <ul class="dropdown-menu">
            {{-- settings --}}
            <li>
              <a class="dropdown-item" href="{{ route('users.edit') }}">Settings</a>
            </li>
            {{-- change password --}}
            <li>
              <a class="dropdown-item" href="{{ route('change-password.edit') }}">Change password</a>
            </li>
            <li>
              <hr class="dropdown-divider">
            </li>
            @hasAnyRoles(['writer'], ['admin'])
            {{-- create new article --}}
            <li>
              <a class="dropdown-item" href="{{ route('articles.create') }}">
                Create new article
              </a>
            </li>
            {{-- articles --}}
            <li>
              <a class="dropdown-item" href="{{ route('articles.table') }}">
                Articles
              </a>
            </li>
            <li>
              <hr class="dropdown-divider">
            </li>
            @endHasAnyRoles

            @hasRole('admin')
            {{-- user --}}
            <li>
              <a class="dropdown-item" href="{{ route('users.index') }}">
                Users
              </a>
            </li>
            {{-- create new category --}}
            <li>
              <a class="dropdown-item" href="{{ route('categories.create') }}">
                Create new category
              </a>
            </li>
            {{-- tags --}}
            <li>
              <a class="dropdown-item" href="{{ route('tags.create') }}">
                Create new tag
              </a>
            </li>
            <li>
              <hr class="dropdown-divider">
            </li>
            @endHasRole
            {{-- logout --}}
            <li>
              <form action="{{ route('logout') }}" method="POST">
                @csrf
                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); this.closest('form').submit()">
                  Logout</a>
              </form>
            </li>
          </ul>
        </li>
        @else
        {{-- login and register --}}
        <li class="nav-item">
          <a class="nav-link {{ (request()->is('login')) ? 'active' : '' }}" href="{{ route('login') }}">
            Login
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link {{ (request()->is('register')) ? 'active' : '' }}" href="{{ route('register') }}">
            Register
          </a>
        </li>
        @endauth
      </ul>
    </div>
  </div>
</nav>
