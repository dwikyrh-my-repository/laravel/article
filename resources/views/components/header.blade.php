{{-- header --}}
<div class="bg-skyline text-white mb-5 border-bottom py-5" style="margin-top: -24px">
  <div class="container">
    <h1 class="text-white">
      @if(request()->is('tags*'))
      # {{ $title }}
      @else
      {{ $title }}
      @endif
    </h1>
    <div class="row">
      <div class="col-md-8">
        <p class="lead text-white">
          {{ $subtitle }}
        </p>
      </div>
    </div>
    <div class="row d-flex align-items-center justify-content-center">
      <div class="col-12 col-md-8">
        <x-search></x-search>
      </div>
    </div>
  </div>
</div>
