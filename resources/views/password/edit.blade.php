<x-app-layout title="Change Password {{ auth()->user()->name }} - GreenK Article">
  @push('styles')
  @include('components.toast.alerts-css')
  @endpush
  <div class="container mt-5">
    <div class="d-flex align-items-center justify-content-center">
      <div class="col-md-6">
        <x-card title="Change Password" subtitle="Don't tell anyone about your secret password." class="shadow">
          {{-- Form Change Password --}}
          <form action="{{ route('change-password.update') }}" method="POST">
            @csrf
            @method('PUT')

            {{-- Password --}}
            <div class="mb-4">
              <label for="current-password" class="form-label">Current Password</label>
              <input type="password" name="current_password" id="current-password" class="form-control @error('current_password') is-invalid @enderror">
              @error('current_password')
              <div class="invalid-feedback">{{ $message }}</div>
              @enderror
            </div>

            {{-- New Password --}}
            <div class="mb-4">
              <label for="password" class="form-label">New Password</label>
              <input type="password" name="password" id="password" class="form-control @error('password') is-invalid @enderror">
              @error('password')
              <div class="invalid-feedback">{{ $message }}</div>
              @enderror
            </div>

            {{-- Confirm Password --}}
            <div class="mb-4">
              <label for="confirm-password" class="form-label">Confirm New Password</label>
              <input type="password" name="password_confirmation" id="confirm-password" class="form-control">
            </div>

            {{-- Button Submit Change Password --}}
            <button type="submit" class="btn btn-primary btn-md">
              Change Password
            </button>
          </form>
        </x-card>
      </div>
    </div>
  </div>
  @push('scripts')
  @include('components.toast.alerts-js')
  @endpush
</x-app-layout>
