<x-app-layout title="{{ auth()->user()->name }} Settings - GreenK Article">
    @push('styles')
    @include('components.toast.alerts-css')
    @endpush
  <div class="container mt-5">
    <div class="row d-flex align-items-center justify-content-center">
      <div class="col-md-8">
        <x-card title="Profile" subtitle="The information you enter will appear on the profile page." class="shadow">

          {{-- form edit user --}}
          <form action="{{ route('users.update', $users) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="row">

              {{-- name --}}
              <div class="col-md-6">
                <div class="mb-4">
                  <label for="name" class="form-label">Name</label>
                  <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror"
                  value="{{ old('name', auth()->user()->name) }}">
                  @error('name')
                  <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
                </div>
              </div>

              {{-- username --}}
              <div class="col-md-6">
                <div class="mb-4">
                  <label for="username" class="form-label">Username</label>
                  <div class="input-group">
                    <span class="input-group-text" id="username">
                      greenk-article.online/
                    </span>
                    <input type="text" name="username" id="username" class="form-control @error('username') is-invalid @enderror"
                    value="{{ old('username', auth()->user()->username) }}">
                    @error('username')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
              </div>
            </div>

            {{-- email address --}}
            <div class="mb-4">
              <label for="email" class="form-label">Email address</label>
              <input value="{{ old('email', auth()->user()->email) }}" type="text" name="email" id="email"
              class="form-control @error('email') is-invalid @enderror">
              @error('email')
              <div class="invalid-feedback">{{ $message }}</div>
              @enderror
            </div>

            {{-- button submit --}}
            <button type="submit" class="btn btn-primary">
              Update
            </button>

          </form>
        </x-card>
      </div>
    </div>
  </div>
  @push('scripts')
  @include('components.toast.alerts-js')
  @endpush
</x-app-layout>
