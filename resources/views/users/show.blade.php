<x-app-layout title="{{ $user->name }} - GreenK Article">
    <x-header title="{{ $user->name }}" subtitle="{{ '@' . $user->username }}">
        <span class="text-white"> Joined {{ $user->created_at->format('d F, Y') }}</span>
    </x-header>
    <div class="container">
        <x-articles :articles="$articles"/>
    </div>
</x-app-layout>
