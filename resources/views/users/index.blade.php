<x-app-layout title="Table of Users - GreenK Article">
    <div class="container">
        {{-- Feedback --}}
        @if (session()->has('status'))
        <div class="alert alert-info" role="alert">
            {{ session('status') }}
        </div>
        @endif
        <x-card title="Users" subtitle="Table of users" class="shadow">
            {{-- Table of Users --}}
            <table class="table table-striped">
                {{-- Thead --}}
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th></th>
                    </tr>
                </thead>
                {{-- Tbody --}}
                <tbody>
                    @forelse ($users as $user)
                    <tr>
                        <td>{{ $users->firstItem() + $loop->index }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->roles->count() ? $user->roles->implode('name', ', ') : '-' }}</td>
                        <td>
                            <div class="d-flex justify-content-end">
                                <div class="dropdown">
                                    <button class="btn border-0 p-0 btn-transparent" type="button" id="user-action" data-bs-toggle="dropdown"
                                    aria-expanded="false">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-three-dots-vertical"
                                        viewBox="0 0 16 16">
                                        <path d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5
                                        1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
                                    </svg>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="user-action">
                                        @foreach ($roles as $role)
                                        <li>
                                            <form method="POST" action="{{ route('roles.assign', $user) }}">
                                                @csrf
                                                <input type="hidden" name="role_id" value="{{ $role->id }}">
                                                <a class="dropdown-item" href="{{ route('roles.assign', $user) }}"
                                                onclick="event.preventDefault();this.closest('form').submit();">
                                                Assign to be {{ $role->name }}
                                                </a>
                                            </form>
                                        </li>
                                        @endforeach
                                        <li><hr class="dropdown-divider"></li>
                                        <li><a class="dropdown-item" href="#">Remove</a></li>
                                    </ul>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="4" class="text-center">Data is currently empty.</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
            {{ $users->links() }}
        </x-card>
    </div>
</x-app-layout>
