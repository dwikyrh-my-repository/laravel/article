<x-app-layout title="{{ $tag->name }} Tag - GreenK Article">
    <x-header title="{{ $tag->name }}" subtitle="This page will show all articles tagged by {{ $tag->name }}" />
    <div class="container">
        <x-articles :articles="$articles" />
    </div>
</x-app-layout>
