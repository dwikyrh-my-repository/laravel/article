@csrf
<div class="mb-4">
  {{-- Tag Name --}}
  <label for="name" class="form-label">Tag Name</label>
  <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name', $tag->name) }}">
  @error('name')
  <div class="invalid-feedback">{{ $message }}</div>
  @enderror
</div>
{{-- Button Submit --}}
<button type="submit" class="btn btn-primary">{{ $submit }}</button>
