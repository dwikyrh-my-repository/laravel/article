<x-app-layout title="Tags - GreenK Article">
  @push('styles')
  @include('components.toast.alerts-css')
  @endpush
  <div class="container">
    <div class="row">
      <div class="d-flex align-items-center justify-content-center">
        <div class="col-md-8">
          <div class="d-flex justify-content-end mb-2">
            <a class="btn btn-primary" href="{{ route('tags.create') }}">New</a>
          </div>
          <x-card title="Tags" subtitle="Table of tags" class="shadow">
            <div class="table-responsive">

              {{-- table of tags --}}
              <table class="table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Slug</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @forelse ($tags as $index => $tag)
                  <tr>
                    <td>{{ $index + 1 }}</td>
                    <td>{{ $tag->name }}</td>
                    <td>{{ $tag->slug }}</td>
                    <td>
                      <a href="{{ route('tags.edit', $tag) }}" class="btn btn-warning btn-sm">
                        Edit
                      </a>
                    </td>
                  </tr>
                  @empty
                  <tr>
                    <td colspan="4" class="alert alert-info">No Tag Data</td>
                  </tr>
                  @endforelse
                </tbody>
              </table>

            </div>
          </x-card>
          {{-- pagination --}}
          {{ $tags->links() }}
        </div>
      </div>
    </div>
  </div>
  @push('scripts')
  @include('components.toast.alerts-js')
  @endpush
</x-app-layout>
