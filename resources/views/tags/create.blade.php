<x-app-layout title="Create New Tag - GreenK Article">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <x-card title="New" subtitle="Create new tag" class="shadow">
            {{-- form create tag --}}
          <form action="{{ route('tags.store') }}" method="POST">
            @include('tags.form', ['submit' => 'Create'])
          </form>
        </x-card>
      </div>

      {{-- table of tags --}}
      <div class="col-md-4">
        <a class="btn btn-primary w-100" href="{{ route('tags.index') }}">
          Table of tags
        </a>
      </div>
    </div>
  </div>
</x-app-layout>
