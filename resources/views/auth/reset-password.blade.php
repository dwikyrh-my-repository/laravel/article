<x-app-layout title="Reset Password - GreenK Article">
  <div class="container">
    <div class="row d-flex align-items-center justify-content-center">
      <div class="col-md-6">
        <x-card title="Reset Password" subtitle="Reset your password">
          {{-- form Reset Password --}}
          <form action="{{ route('password.update') }}" method="POST">
            @csrf
            <input type="hidden" name="token" value="{{ $token }}">

            {{-- email address --}}
            <div class="mb-4">
              <input type="email" name="email" id="email" value="{{ $email }}"
              class="form-control @error('email') is-invalid @enderror" placeholder="Email Address">
              @error('email')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
              @enderror
            </div>

            {{-- new password --}}
            <div class="mb-4">
              <input type="password" name="password" id="password" class="form-control @error('password') is-invalid @enderror"
              placeholder="New Password">
              @error('password')
              <div class="invalid-feedback">{{ $message }}</div>
              @enderror
            </div>

            {{-- new password confirmation --}}
            <div class="mb-4">
              <input type="password" name="password_confirmation" id="confirm-password" class="form-control"
              placeholder="New Password Confirmation">
            </div>

            {{-- button submit --}}
            <button type="submit" class="btn btn-primary">
              Reset Password
            </button>
          </form>
        </x-card>
      </div>
    </div>
  </div>
</x-app-layout>
