<x-app-layout title="Register - GreenK Article">
  <section id="register">
    <div class="container">
      <div class="row g-0 d-flex align-items-center justify-content-center">
        <div class="col-12 col-md-6 col-lg-4">
          <div class="card-body py-4 px-md-4 shadow rounded mt-2 mt-md-3" style="height: 520px;">
            {{-- header text --}}
            <h5 class="text-center fw-bold fs-4">REGISTER</h5>

            {{-- form register --}}
            <form action="{{ route('register') }}" method="POST" autocomplete="off" class="px-4 px-lg-2">
              @csrf
              {{-- name --}}
              <div class="mb-4 mt-4">
                <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}" placeholder="Name">
                @error('name')
                <div class="invalid-feedback">
                  <strong>{{ $message }}</strong>
                </div>
                @enderror
              </div>

              {{-- email address --}}
              <div class="mb-4">
                <input type="email" name="email" id="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" placeholder="Email Address">
                @error('email')
                <div class="invalid-feedback">
                  <strong>{{ $message }}</strong>
                </div>
                @enderror
              </div>

              {{-- password --}}
              <div class="mb-4">
                <input type="password" name="password" id="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password">
                @error('password')
                <div class="invalid-feedback">
                  <strong>{{ $message }}</strong>
                </div>
                @enderror
              </div>

              {{-- password confirmation --}}
              <div class="mb-4">
                <input type="password" name="password_confirmation" id="confirm-password" class="form-control" placeholder="Confirm Password">
              </div>

              {{-- button submit --}}
              <div class="mt-4">
                <button type="submit" class="btn btn-primary btn-md w-100">Register</button>
              </div>
            </form>

            {{-- button to login --}}
            <div class="mt-3 text-center">
              <span>Already have an account?<a href="{{ route('login') }}"> Login</a></span>
            </div>

          </div>
        </div>
      </div>
    </div>
  </section>
</x-app-layout>
