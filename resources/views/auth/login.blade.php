@push('styles')
<link rel="stylesheet" href="{{ asset('assets/css/auth/login-register.css') }}">
@endpush
<x-app-layout title="Login - GreenK Article">
  <section id="login">
    <div class="container">
      <div class="row g-0 d-flex align-items-center justify-content-center">
        <div class="col-12 col-md-6 col-lg-4">
          <div class="card-body py-4 px-md-4 shadow rounded mt-2 mt-md-3" style="height: 520px;">
            {{-- header text --}}
            <h5 class="text-center fw-bold fs-4" style="margin-top: 65px">LOGIN</h5>
            @if(session('status'))
                <div class="alert alert-success">{{ session('status') }}</div>
            @endif
            {{-- form login --}}
            <form action="{{ route('login') }}" method="POST" autocomplete="off" class="px-4 px-lg-2">
              @csrf

              {{-- email address --}}
              <div class="mb-4 mt-4">
                <input type="text" name="email" id="email" class="form-control @error('email') is-invalid @enderror"
                value="{{ old('email') }}" placeholder="Email Address">
                @error('email')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
              </div>

              {{-- password --}}
              <div class="mb-4">
                <input type="password" name="password" id="password" class="form-control
                @error('password') is-invalid @enderror" value="{{ old('password') }}" placeholder="Password">
                @error('password')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
              </div>

              {{-- remember me --}}
              <div class="d-flex flex-wrap justify-content-between align-items-center mb-sm-4">
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" name='remember' id="remember-me">
                  <label class="form-check-label" for="remember-me">
                    Remember me
                  </label>
                </div>
                <a class="text-muted" href="{{ route('password.request') }}">
                  ForgotPassword
                </a>
              </div>

              {{-- button submit --}}
              <button type="submit" class="btn btn-primary btn-md w-100 mt-2 mt-sm-0">Login</button>
            </form>

            {{-- button to register --}}
            <div class="mt-3 mx-4 text-center">
              <span>
                Don't have an account yet? <a href="{{ route('register') }}">Register</a>
              </span>
            </div>

          </div>
        </div>
      </div>
    </div>
  </section>
</x-app-layout>
