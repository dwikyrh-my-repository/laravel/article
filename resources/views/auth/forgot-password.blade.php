<x-app-layout title="Forgot Password - GreenK Article">
  <div class="container">
    <div class="row">
      <div class="d-flex align-items-center justify-content-center">
        <div class="col-md-6">
          <x-card title="Forgot Password" subtitle="Enter your email address and we'll send verification link." class="shadow mt-4">
            {{-- feedback --}}
            @if(session('status'))
            <div class="alert alert-success">
              {{ session('status') }}
            </div>
            @endif

            {{-- form reset password --}}
            <form action="{{ route('password.email') }}" method="POST">
              @csrf
              {{-- email --}}
              <div class="mb-4">
                <div class="mb-4">
                  <input type="text" name="email" id="email" class="form-control @error('email') is-invalid @enderror"
                  placeholder="Email Address">
                  @error('email')
                  <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
                </div>
              </div>
              {{-- Button Submit --}}
              <button type="submit" class="btn btn-primary">Forgot Password</button>
            </form>

          </x-card>
        </div>
      </div>
    </div>
  </div>
</x-app-layout>
