<x-app-layout title="Verify Email - GreenK Article">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <x-card title="Verify Email" subtitle="Resend the link notification">
                    {{-- Feedback --}}
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif
                    <p>
                        Thanks for signing up! Before getting started, could you
                        verify your email address by clicking on the link we just
                        emailed to you? If you didn't receive the email, we will
                        gladly send you another.
                    </p>
                    {{-- Form Resend Emaul Verify --}}
                    <form action="{{ route('verification.send') }}" method="POST">
                        @csrf
                        <button type="submit" class="btn btn-primary btn-md">
                            Resend
                        </button>
                    </form>
                </x-card>
            </div>
        </div>
    </div>
</x-app-layout>
