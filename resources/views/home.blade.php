<x-app-layout title="Home - GreenK Article">
  <x-header title="Home">
    @slot('subtitle')
    Welcome to GreenK Article {{ $user ?? '' }}
    @endslot
  </x-header>
  <div class="container">
    <h4 class="lead fw-bold fs-3 mb-2">New Articles</h4>
    <x-articles :articles="$articles" />
  </div>
</x-app-layout>
