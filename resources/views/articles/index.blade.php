<x-app-layout title="All Articles - GreenK Article">
  @push('styles')
  @include('components.toast.alerts-css')
  @endpush
  <x-header title="Articles">
    @slot('subtitle')
    Find your favorite article only here
    @endslot
  </x-header>
  <div class="container">
    <x-articles :articles="$articles" />
  </div>
  @push('scripts')
  @include('components.toast.alerts-js')
  @endpush
</x-app-layout>
