<x-app-layout title="New article - GreenK Article">
  <div class="container">
    <x-card class="mb-4" title="New" subtitle="Create new article" class="shadow">
      {{-- form create article --}}
      <form action="{{ route('articles.store') }}" method="POST" enctype="multipart/form-data">
        @include('articles.form', ['submit' => 'Create'])
      </form>
    </x-card>
  </div>
</x-app-layout>
