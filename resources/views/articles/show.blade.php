<x-app-layout title="{{ $article->title }} - GreenK Article">
    @push('styles')
    @include('components.toast.alerts-css')
    @endpush
  <div class="bg-skyline text-white mb-5 border-bottom py-5" style="margin-top: -24px">
    <div class="container">
      <div class="d-flex align-items-center gap-5 py-4 px-2">
        <div class="col-lg-7">
          <div class="me-auto">
            <div class="d-flex align-items-center gap-2 mb-3">
              {{-- category --}}
              <a href="{{ route('categories.show', $article->category) }}" class="btn btn-primary btn-sm">
                {{ $article->category->name }}
              </a>
              {{-- tags --}}
              @foreach ($article->tags as $tag)
              <a href="{{ route('tags.show', $tag) }}" class="btn btn-dark btn-sm">
                #{{ $tag->name }}
              </a>
              @endforeach
            </div>
            {{-- article title --}}
            <h1 class="display-6 fw-semibold mt-2">{{ $article->title }}</h1>
            <hr>

            {{-- article body (header) --}}
            <p class="text-align lead">{!! str($article->body)->limit(150) !!}</p>
            <div class="d-flex align-items-center justify-content-between mt-4">
              <div class="text-white-50">
                {{-- published / created_at and user--}}
                {{ $article->created_at->format('d F, Y') }} by
                {{-- user / writer --}}
                <a href="{{ route('users.show', $article->user) }}" class="text-white">
                  {{ $article->user->name }}
                </a>
                &middot;
                {{-- likes count --}}
                <span>
                  {{ $article->likes_count }} {{ str('like')->plural($article->likes_count) }}
                </span>
              </div>
              <div class="d-flex align-items-center gap-2">

                {{-- button article like --}}
                @auth
                <form action="{{ route('articles.like', $article) }}" method="POST">
                  @csrf
                  <a href="{{ route('articles.like', $article) }}"
                  class="btn btn-sm btn-light" onclick="event.preventDefault();this.closest('form').submit()">
                    {{ $article->alreadyLiked() ? 'Unlike' : 'Like' }}
                  </a>
                </form>
                @endauth

                {{-- button edit article --}}
                @can('update', $article)
                <a href="{{ route('articles.edit', $article) }}" class="btn btn-light btn-sm">
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil" viewBox="0 0 16 16">
                    <path d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z" />
                  </svg>
                </a>
                @endcan
              </div>
            </div>

          </div>
        </div>

        {{-- picture article --}}
        <div class="col-lg-5" id="image-article">
          <img src="{{ (substr($article->picture, 0, 5) == 'https') ? $article->picture : asset('storage/' . $article->picture ) }}" alt="" class="w-80 rounded ms-2 img-fluid d-none d-lg-block" style="max-height: 225px; width:400px">
        </div>

      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-md-8">

        {{-- article body --}}
        <div class="article-body">
          {!! str($article->body)->markdown() !!}
        </div>

        {{-- comments --}}
        <x-articles.comment.block :comments="$comments" />
        <x-articles.comment.form :article="$article" :action="route('comments.store', $article)" method="POST" />
      </div>

      {{-- related articles --}}
      @if (count($relatedArticles) > 0)
      <div class="col-md-4 text-center">
        <h4 class="fw-bolder border-bottom border-4">Related Articles</h4>

        <ul class="list-group list-group-flush">
          @foreach($relatedArticles->shuffle() as $article)
          <li class="list-group-item">
            <a href="{{ route('articles.show', $article) }}" class="text-decoration-none">
              {{ $article->title }}
            </a>
          </li>
          @endforeach
        </ul>
      </div>
      @endif

    </div>
  </div>
  @push('scripts')
  @include('components.toast.alerts-js')
  @endpush
</x-app-layout>
