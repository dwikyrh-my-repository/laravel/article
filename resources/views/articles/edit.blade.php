<x-app-layout title="Edit Article {{ $article->title }} - GreenK Article">
  <div class="container">
    <x-card title="mb-4" title="Edit Article" subtitle="{{ $article->title }}" class="shadow">
      {{-- form edit article --}}
      <form action="{{ route('articles.update', $article) }}" method="POST" enctype="multipart/form-data">
        @method('PUT')
        @include('articles.form', ['submit' => 'Update'])
      </form>
    </x-card>

    {{-- delete button modal --}}
    <div class="text-end mt-2">
      <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#exampleModal">
        Delete
      </button>
    </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title" id="exampleModalLabel">{{ $article->title }}</div>
            <button type="button" class="btn-close" data-bs-dismiss="modal" arialabel="Close"></button>
          </div>
          <div class="modal-body">
            <p class="text-muted text-center">Are you really sure you want to delete
              it?</p>
            <div class="d-flex align-items-center gap-2 justify-content-center">
              <form action="{{ route('articles.destroy', $article) }}" method="POST">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger">
                  Yes
                </button>
              </form>
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" aria-label="Close">No</button>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
  <br>
</x-app-layout>
