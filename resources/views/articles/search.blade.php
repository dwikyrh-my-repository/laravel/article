<x-app-layout title="All Articles - GreenK Article">
  <x-header title="Articles">
    @slot('subtitle')
    Your search results for <span class="fw-bold">"{{ request()->q }}"</span>
    @endslot
  </x-header>
  <div class="container">
    <x-articles :articles="$articles" />
  </div>
</x-app-layout>
