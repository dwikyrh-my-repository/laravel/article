<x-app-layout title="Table of Articles - GreenK Article">
  <div class="container">
    <x-card title="Table of Articles"
    subtitle="Hello {{ auth()->user()->name }}, you can approve or reject articles to be published" class="shadow">

      <div class="table-responsive">
        <table class="table table-striped">
          <thead>
            <th>#</th>
            <th>Title</th>
            <th>Status</th>
            <th>Author</th>
            <th>Created at</th>
            <th></th>
          </thead>

          <tbody>
            @foreach ($articles as $article)
            <tr>
              <td>{{ $articles->firstItem() + $loop->index }}</td>
              <td>
                <a href="{{ route('articles.show', $article) }}" class="text-decoration-none">
                  {{ $article->title }}
                </a>
              </td>
              {{-- article status --}}
              <td>
                @if ($article->status->name == 'PUBLISHED')
                <span class="badge bg-success">
                  {{ $article->status->name }}
                </span>
                @elseif ($article->status->name == 'PENDING')
                <span class="badge bg-warning">
                  {{ $article->status->name }}
                </span>
                @else
                <span class="badge bg-danger">
                  {{ $article->status->name }}
                </span>
                @endif
              </td>
              <td>{{ $article->user->name }}</td>
              <td>{{ $article->created_at->format('d F, Y') }}</td>
              @hasRole('admin')
              <td>
                <x-table.dropdown-menu :article="$article">
                  @if($article->status == \App\Enums\ArticleStatus::PENDING)
                  <li>
                    {{-- APPROVE --}}
                    <form action="{{ route('articles.update-status', $article) }}" method="POST">
                      @csrf @method('PUT')
                      <input type="hidden" name="status" value="{{ \App\Enums\ArticleStatus::PUBLISHED->value }}">
                      <a href="{{ route('articles.update-status', $article) }}" class="dropdown-item" onclick="event.preventDefault();
                                                    this.closest('form').submit()"> Approve
                      </a>
                    </form>
                  </li>
                  {{-- REJECTED --}}
                  <li>
                    <form action="{{ route('articles.update-status', $article) }}" method="POST">
                      @csrf @method('PUT')
                      <input type="hidden" name="status" value="{{ \App\Enums\ArticleStatus::REJECTED->value }}">
                      <a href="{{ route('articles.update-status', $article) }}" class="dropdown-item" onclick="event.preventDefault();
                                                    this.closest('form').submit()"> Rejected
                      </a>
                    </form>
                  </li>
                  @endif
                  {{-- PENDING --}}
                  @if($article->status == \App\Enums\ArticleStatus::REJECTED)
                  <li>
                    <form action="{{ route('articles.update-status', $article) }}" method="POST">
                      @csrf @method('PUT')
                      <input type="hidden" name="status" value="{{ \App\Enums\ArticleStatus::PENDING->value }}">
                      <a href="{{ route('articles.update-status', $article) }}" class="dropdown-item" onclick="event.preventDefault();
                                                    this.closest('form').submit()"> Pending
                      </a>
                    </form>
                  </li>
                  {{-- TAKEDOWN --}}
                  <li>
                    <form action="{{ route('articles.update-status', $article) }}" method="POST">
                      @csrf @method('PUT')
                      <input type="hidden" name="status" value="{{ \App\Enums\ArticleStatus::TAKEDOWN->value }}">
                      <a href="{{ route('articles.update-status', $article) }}" class="dropdown-item" onclick="event.preventDefault();
                                                    this.closest('form').submit()"> Takedown
                      </a>
                    </form>
                  </li>
                  @endif
                  {{-- TAKE DOWN --}}
                  @if($article->status == \App\Enums\ArticleStatus::PUBLISHED )
                  <li>
                    <form action="{{ route('articles.update-status', $article) }}" method="POST">
                      @csrf @method('PUT')
                      <input type="hidden" name="status" value="{{ \App\Enums\ArticleStatus::TAKEDOWN->value }}">
                      <a href="{{ route('articles.update-status', $article) }}" class="dropdown-item" onclick="event.preventDefault();
                                                    this.closest('form').submit()"> Take Down
                      </a>
                    </form>
                  </li>
                  @endif
                </x-table.dropdown-menu>
              </td>
              @endHasRole
              @hasRole('writer')
              <td></td>
              @endHasRole
            </tr>
            @endforeach
          </tbody>
        </table>

      </div>
      {{-- pagination --}}
      {{ $articles->links() }}
    </x-card>
  </div>
</x-app-layout>
