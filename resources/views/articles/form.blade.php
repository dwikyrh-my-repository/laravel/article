@push('styles')
{{-- select2 --}}
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.3.0/dist/select2-bootstrap-5-theme.min.css" />
{{-- ckeditor 4 --}}
<link rel="stylesheet" href="{{ asset('assets/css/articles/ckeditor.css') }}">
@endpush

{{-- form --}}
@csrf
<div class="row">
  <div class="col-md-4">
    {{-- title --}}
    <div class="mb-2">
      <label for="title" class="form-label">Title</label>
      <input type="text" name="title" id="title" value="{{ old('title', $article->title) }}" class="form-control @error('title') is-invalid @enderror" placeholder="Title">
      @error('title')
      <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div>
    
    {{-- picture --}}
    <div class="mb-2">
      <label for="picture" class="form-label">Picture</label>
      <input type="file" name="picture" id="picture" class="form-control @error('picture') is-invalid @enderror">
      @error('picture')
      <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div>

    {{-- category --}}
    <div class="mb-2">
      <label for="category" class="form-label">Category</label>
      <select name="category" id="category" class="form-control @error('category') is-invalid @enderror">
        <option selected disabled>Choose category</option>
        @foreach ($categories as $category)
        <option {{ old('category')==$category->id || $article?->category_id == $category->id ? 'selected' : ''}} value="{{ $category->id }}">
          {{ $category->name }}
        </option>
        @endforeach
      </select>
      @error('category')
      <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div>

    {{-- tags --}}
    <div class="mb-2">
      <label for="tags" class="form-label">Tags</label>
      <select name="tags[]" id="tags" class="form-control @error('tags') is-invalid @enderror" multiple>
        @foreach ($tags as $tag)
        <option {{ ($article?->tags()->find($tag->id)) || (collect(old('tags'))->contains($tag->id)) ?
                    'selected' : '' }} value="{{ $tag->id }}">{{ $tag->name }}</option>
        @endforeach
      </select>
      @error('tags')
      <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div>
  </div>

  <div class="col-md-8">
    {{-- body --}}
    <div class="mb-2">
      <label for="body" class="form-label">Body</label>
      <textarea name="body" id="body" class="form-control @error('body') is-invalid @enderror">{{ old('body', $article->body) }}</textarea>
      @error('body')
      <div class="invalid-feedback">{{ $message }}</div>
      @enderror
    </div>
  </div>
</div>

{{-- button submit --}}
<div class="text-end">
  <button type="submit" class="btn btn-primary btn-md">{{ $submit }}</button>
</div>

@push('scripts')
{{-- select2 --}}
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.0/dist/jquery.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
  $('#tags,#category').select2({
    theme: 'bootstrap-5'
  });

</script>
{{-- ck editor 4 --}}
<script src="{{ asset('assets/js/ckeditor/ckeditor.js') }}"></script>
<script>
  CKEDITOR.replace('body', {
    language: 'en'
    , height: 400
  , });

</script>
@endpush
